package com.randomimage.app.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.randomimage.app.model.Images;
import com.randomimage.app.service.RandomImageService;

@RestController
public class RandomImageController {
	
	
	@Autowired
	RandomImageService randomImageService;

	private static final String URI = "https://picsum.photos/200/300";
	
	private RestTemplate restTemplate;
	
	@GetMapping(value  = "/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] imageById(@PathVariable("id") String id) {
		restTemplate = new RestTemplate();
		if(randomImageService.isImageExists(id)) {
			return randomImageService.getImageById(id);
		}else {
			byte[] response = null;
			  try {
			    restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
			    response = this.restTemplate.exchange(URI, HttpMethod.GET, null, byte[].class).getBody();
			    return randomImageService.persistImage(id, response).getImage();
			  } catch( HttpServerErrorException hse ){
			    throw hse;
			  }
		}
		 
	}
	
	@GetMapping(value = "/", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage() {
		restTemplate = new RestTemplate();
		 byte[] response = null;
		  try {
		    restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
		    response = this.restTemplate.exchange(URI, HttpMethod.GET, null, byte[].class).getBody();
		    return response;
		  } catch( HttpServerErrorException hse ){
		    throw hse;
		  }
	}

	@GetMapping(value = "/images")
	public ResponseEntity<List<Images>> getAllImages(){
	   return new ResponseEntity<>(this.randomImageService.getAllImages(), HttpStatus.OK);
	}
}
