package com.randomimage.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.randomimage.app.model.Images;

@Repository
public interface ImageRepository extends JpaRepository<Images, Integer>{

}
