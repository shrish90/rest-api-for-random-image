package com.randomimage.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomImageApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomImageApiApplication.class, args);
	}

}
