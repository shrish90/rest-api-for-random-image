package com.randomimage.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.randomimage.app.model.Images;
import com.randomimage.app.repository.ImageRepository;

@Service
public class RandomImageService {

    @Autowired
    ImageRepository imageRepository;

    public boolean isImageExists(String id) {
        return imageRepository.existsById(Integer.parseInt(id));
    }

    public Images persistImage(String id, byte[] image) {
        Images imageEntity = new Images();
        imageEntity.setId(Integer.parseInt(id));
        imageEntity.setImage(image);
        return imageRepository.save(imageEntity);
    }

    public byte[] getImageById(String id) {
        return imageRepository.findById(Integer.parseInt(id)).get().getImage();
    }

    public List<Images> getAllImages() {
        return imageRepository.findAll();
    }
}
